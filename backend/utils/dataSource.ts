import { DataSource } from 'apollo-datasource';
import { DocumentStore, IDocumentSession } from 'ravendb';

class RavenDBDataSource extends DataSource {

    private documentStore: DocumentStore;
    private session: IDocumentSession;

    constructor(store: DocumentStore) {
        super();
        this.documentStore = store;
    }

    initialize(): void {
        this.session = this.documentStore.openSession();
    }

    getSession(): IDocumentSession {
        return this.session;
    }

}

export default RavenDBDataSource;
