import React, { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import gql from "graphql-tag";
import './PostDetail.css'
import NewPost from '../newPost/NewPost';
import VoteComponent from '../voteComponent/VoteComponent'


const GET_POST = gql`
query GetPost($id: String!) {
  post(id: $id) {
    id
    text
    parent
    creationDate
    likes
    children {
      id
      text
      creationDate
      likes
      user {
        username
      }
      parent
    }
    user {
      username
    }
  }
}`


export default function PostDetail(props) {

  let [ showReply, toggleReply ] = useState(false);
  let [ postData, setPost ] = useState(0);
  
  let id = '';
  if(props.postId) {
    id = props.postId;
  } else if(props.location && props.location.state) id = props.location.state.id;

  const query = useQuery(GET_POST, {
    skip: !id,
    variables: {
      id: id,
    },
    fetchPolicy: "cache-and-network",
    onCompleted( { post } ) {
      setPost(post)
    },
    onError( err ) {
      alert(err.message)
      return err
    }
  });


  if (query.loading) return <p>Loading...</p>;
  if (query.error) return <p>Error</p>;
    
  return (
    
    <div className="container" style={{border: '1px solid black', padding: '10px'}}>
      {postData &&
      <div>
        <div className="row card" style={{wordBreak: 'break-all', margin: '20px'}}>
          <div className="col-2">
            <VoteComponent postId={postData.id} likes={postData.likes}></VoteComponent>
          </div>
          <div className="col-9">
            <div className="row">
              <div className="col-12">
                <small className="form-text text-muted">Posted by <span className={postData.user.username ? 'user-tag':null}>{postData && postData.user &&
                  (postData.user.username ? postData.user.username : ' Anonymous')}</span> {' at ' + postData.creationDate}</small>
              </div>
            </div>
            <div className="row">
              <div className="col-11">
                <div style={{margin: '20px 0px', fontSize: '20px'}}>{postData.text}</div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <small>{postData.creationDate}</small>
              </div>
            </div>
          </div>
        </div>
        <div className="row" style={{justifyContent: 'center', margin: '20px'}}>
          <div className="col-3">
            <button type="button" className="btn btn-primary" onClick={() => toggleReply(() => {
              return !showReply
            })}>Responder</button>
          </div>
        </div>
        {showReply &&
          <div className="row" style={{justifyContent: 'center'}}>
            <div className="col-9">
              <NewPost parentId={postData.id}></NewPost>
            </div>
          </div>
        }
        {postData.children && postData.children.length > 0 &&
          postData.children.map((post, index) => (
            <div key={index}>
              <PostDetail postId={post.id}></PostDetail>
            </div>          
          ))
        }
      </div>
      }
    </div>
    
  )
    
}