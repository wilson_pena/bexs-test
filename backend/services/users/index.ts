import { ApolloServer, gql }  from 'apollo-server';
import { buildFederatedSchema } from '@apollo/federation';
import { createRavenDatabase, getStore } from '../../utils/dbUtils';
import RavenDBDataSource from '../../utils/dataSource';
import UserEntity from './entities/User';


export const ravenAddress = process.env.RAVEN_ADDRESS 
  ? (process.env.RAVEN_ADDRESS)  
  : 'http://localhost:8080'

export const databaseName = process.env.DATABASE_NAME ? process.env.DATABASE_NAME : 'bexs-users'

const typeDefs = gql`
  type Mutation {
    logInAs (username: String!, password: String!): User!
  }

  type User @key(fields: "id") {
    id: ID!
    username: String
  }
`;

const resolvers = {
  User: {
    __resolveReference(user, { dataSources: { ravenDataSource } }){
      return UserEntity.fetchById(user.id, ravenDataSource.getSession());
    }
  },
  Mutation: {
    logInAs: (_, { username, password } , { dataSources: { ravenDataSource } }) => {
      return  UserEntity.createUser(username, password, ravenDataSource.getSession())
    },
  },
};

createRavenDatabase(ravenAddress, databaseName);
const store = getStore(ravenAddress, databaseName);

const server = new ApolloServer({
  dataSources: () => { 
    return { ravenDataSource: new RavenDBDataSource(store)}
  },
  schema: buildFederatedSchema([
    {
      typeDefs,
      resolvers
    }
  ])
});

const host = process.env.USERS_HOST ? process.env.USERS_HOST : 'localhost'
const port = process.env.USERS_PORT ? process.env.USERS_PORT : 4002

server.listen({ host, port }).then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
