
import { createRavenDatabase, getStore, deleteDatabase } from '../../../utils/dbUtils';
import UserEntity from '../entities/User';
import { SinonFakeTimers, useFakeTimers } from 'sinon';


const testDatabaseAddress = 'http://localhost:8080' ;
const databaseName = 'bexs-test';

describe('test users', () => {
  const store = getStore(testDatabaseAddress, databaseName);
  const mockDate = new Date();
  let clock: SinonFakeTimers = useFakeTimers(mockDate);

  beforeAll(async () => {
    await createRavenDatabase(testDatabaseAddress, databaseName);
  })
  
  afterAll(async () => {
    await deleteDatabase(testDatabaseAddress, databaseName);
  })
  
  afterAll(() => {
    store.dispose();
    clock.restore();
  })

  test('log in as new user', async () => {
    const session = store.openSession()
    const newUser = await UserEntity.createUser('username1', 'pass', session);
    session.dispose();

    const User = {
        username: 'username1',
    }

    expect(newUser).toMatchObject(User);
  });

  test('log in as created user', async () => {
    const session = store.openSession()
    const newUser = await UserEntity.createUser('username1', 'pass', session);
    session.dispose();

    const User = {
        username: 'username1',
    }

    expect(newUser).toMatchObject(User);
  });

  test('log in with wrong password', async () => {
    const session = store.openSession()
    await UserEntity.createUser('username10', 'pass1', session);
    await expect(async () => {
      await UserEntity.createUser('username1', 'pass123', session);
    }).rejects.toThrow(new Error('Invalid password!'))

    session.dispose();
  });


  test('get by id', async () => {
    const session = store.openSession()
    const fetchedUser = await UserEntity.fetchById("users/1-A", session);
    session.dispose();
    
    const User = {
        id: "users/1-A",
        username: 'username1'
    }
    expect(fetchedUser).toMatchObject(User);
  });

  test('get by invalid id', async () => {
    const session = store.openSession()
    const fetchedUser = await UserEntity.fetchById("posts/2-A", session);
    session.dispose();

    expect(fetchedUser).toBeNull();
  });
  
})
