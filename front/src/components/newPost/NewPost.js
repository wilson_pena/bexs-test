import React, { useState } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { useHistory } from 'react-router-dom';
import gql from "graphql-tag";
import './NewPost.css'

const CREATE_POST = gql`
mutation CreatePost($text: String!, $user: String, $parent: String) {
  createPost(post: {
    text: $text
    user: $user
    parent: $parent
  }) {
    id
    text
    user {
      username
    }
  }
}`

export default function NewPost({props, parentId}) {
  
  let [ post, submitPost ] = useState({});

  const history = useHistory();

  const [
    createPost,
  ] = useMutation(CREATE_POST);
    
  return (
    <div className="container">
      <div className="row" style={{justifyContent: 'center'}}>
        <div className="col-9">
          <div className="form-group">
          {post &&
          post.text}
            <label>Post Text</label>
            <textarea 
              value={post.text} 
              className="form-control" 
              style={{minHeight: '100px'}}
              onChange={(event) => submitPost(() => {
                event.persist()
                post.text= event.target.value
                return post
              })}
            >
            </textarea>
            </div>
          </div>
        </div>
        <div className="row" style={{justifyContent: 'center'}}>
          <button className="btn btn-primary" onClick={(e) => submitPost(async () => {
            try {
              e.persist();
              if(!post.text) {
                alert('Text is empty')
                return post;
              }
              post.text = post.text.replace(/\r?\n/g, '<br />');
              await createPost({ variables: {
                text: post.text,
                user: sessionStorage.getItem('userId') ? sessionStorage.getItem('userId') : '',
                parent: parentId ? parentId : ''
              }});
              if(!parentId) history.push('/')
              window.location.reload()
            } catch(ex) {
              console.log(ex)
            }

          })}>Submit Post</button>
        </div>
      </div>
  )
    
}