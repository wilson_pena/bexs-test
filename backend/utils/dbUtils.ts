import { DocumentStore, CreateDatabaseOperation, GetStatisticsOperation, DeleteDatabasesOperation } from "ravendb";

export const createRavenDatabase = async (ravenAddress, databaseName) => {
  const documentStore = new DocumentStore(ravenAddress, databaseName);
  documentStore.initialize();
  try {
    await documentStore.maintenance.forDatabase(databaseName).send(new GetStatisticsOperation())
  } catch(err) {
    if (err.name === 'DatabaseDoesNotExistException') {
        const dbOperation = new CreateDatabaseOperation({databaseName: databaseName});
        try {
            await documentStore.maintenance.server.send(dbOperation);
        } catch(ex) {
            
        }
    }
  }

  documentStore.dispose();
}

export const deleteDatabase = async (address, databaseName) => {
  const store = new DocumentStore(address, databaseName);
  store.initialize();

  try {
    const deleteDatabaseOperation = new DeleteDatabasesOperation({
      databaseNames: [databaseName],
      hardDelete: true,
    })
    await store.maintenance.send(deleteDatabaseOperation);
  } catch(err) {
    if(err.name !== 'DatabaseDoesNotExistException') 
      console.log('\nError removing DB: ', err);
  }

  store.dispose();
}

export const dispose = ():void => {

} 

export const getStore = (ravenAddress, databaseName): DocumentStore => {
    const documentStore =  new DocumentStore(ravenAddress, databaseName);
    documentStore.initialize();
    return documentStore;
}