import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import gql from "graphql-tag";
import UserLogin from '../user/UserLogin'
import VoteComponent from '../voteComponent/VoteComponent';
import './MainPage.css'

const GET_POSTS = gql`
query PaginatedPosts($pageSize: Int, $pageNumber: Int) {
  paginatedPosts(pageSize: $pageSize, pageNumber: $pageNumber) {
    page {
      id
      text
      creationDate
      likes
      children {
        id
      }
      user {
        username
      }
    }
    totalCount
    totalPages
  }
}`

export default function MainPage() {
    
  let [currPage, nextPage] = useState(0);
  let [pageSize, changePageSize] = useState(10);

  const { data, loading, error } = useQuery(GET_POSTS, {
    variables: {
      pageNumber: currPage,
      pageSize: pageSize,
    },
  });

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error - {error.message}</p>;

  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="col-9">
          <div className="form-group">
            <label >Posts por página:</label>
            <select className="form-control" defaultValue={pageSize} onChange={(ev) => {
                ev.persist()
                changePageSize(parseInt(ev.target.value))
              }}>
              <option value={5}>5</option>
              <option  value="10">10</option>
              <option value="20">20</option>
            </select>
          </div>

          {data && data.paginatedPosts && 
            data.paginatedPosts.page.map((post, index) => (
              <div key={index} className="card card-main" style={{cursor: 'pointer'}}>
                <VoteComponent postId={post.id} likes={post.likes}></VoteComponent>
                  <Link to={{ pathname: '/post-detail', state: { id: post.id} }}>
                    <div className="card-body" style={{wordBreak: 'break-word'}}>
                      <div className="row">
                        <small className="form-text text-muted">Posted by <span className={post.user.username ? 'user-tag':null}>
                          {post && post.user &&
                            (post.user.username ? post.user.username : ' Anonymous')}</span> {' at ' + post.creationDate}</small>
                      </div>
                      <div className="row">
                        <div>{post.text}</div>
                      </div>
                      <div className="row">
                        <small className="form-text text-muted">Comments {post && post.children &&
                          post.children.length}</small>
                      </div>
                    </div>
                 </Link> 
              </div>  
          ))}
          </div>
          <div className="col-3">
            <UserLogin></UserLogin>
          </div>
        </div>
      </div>
      <footer 
        className="row fixed-bottom bg-light" 
        style={{justifyContent: 'center', padding: '20px', position: 'sticky'}}
      >
        <div  style={{cursor: 'pointer'}} className="col-2">
          <div onClick={(event) => nextPage(() => {
            event.persist();
            if(currPage <= 0) {
              currPage = 0;
            } else {
              currPage = currPage - 1;
            }
            return currPage;
          })}>Prev. Page</div>
        </div>
        <div className="col-2">
        {data &&
            [ ...Array(data.paginatedPosts.totalPages).keys() ].map( i => i+1).map(v => (
            <span 
              key={v}
              className="page-number"
              style={v-1 === currPage ? {fontWeight: 'bold'} : {}} 
              onClick={() => nextPage(() => {
                return currPage = v-1
              })}>{v}
            </span>
            )
        )}                
        </div>
        <div style={{cursor: 'pointer'}} className="col-2">
          <div onClick={() => nextPage(() => {
            if(data.paginatedPosts.totalPages <= (currPage+1)) {
              currPage = 0;
            } else {
              currPage = currPage + 1;
            }
            return currPage
          })}>Next Page</div>
        </div>
      </footer>
    </div>
  )
    
}