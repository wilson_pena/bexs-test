import { ApolloServer } from 'apollo-server';
import { ApolloGateway } from '@apollo/gateway';

export const postsURL = process.env.POSTS_HOST 
  ? (process.env.POSTS_HOST + ':' + process.env.POSTS_PORT)  
  : 'http://localhost:4001';

export const usersURL = process.env.USERS_HOST 
  ? (process.env.USERS_HOST + ':' + process.env.USERS_PORT)  
  : 'http://localhost:4002';

const gateway = new ApolloGateway({
  serviceList: [
    { name: "posts", url: postsURL + '/graphql' },
    { name: "users", url: usersURL + '/graphql' },
  ],
  __exposeQueryPlanExperimental: false,
});



(async () => {
  const server = new ApolloServer({
    gateway,
    engine: false,
    subscriptions: false,
  });

  server.listen().then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
  })
})();
