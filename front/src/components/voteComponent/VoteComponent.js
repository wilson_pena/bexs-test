import React, { useState } from 'react';
import { useMutation } from '@apollo/react-hooks';
import gql from "graphql-tag";

const VOTE_POST = gql`
mutation VotePost($postId: String!, $user: String!) {
  votePost(input: {
    user: $user,
    postId: $postId
    }) {
      id
      user {
        username
      }
      likes
    }
  }
`;

export default function VoteComponent(props) {

  let [ votes, changeVote ] = useState(props.likes);

  const [
    votePost,
  ] = useMutation(VOTE_POST, {
    onCompleted( { votePost } ) {
      changeVote(votePost.likes);
    },
    onError( err ) {
      alert(err.message)
      return err
    }
  });

  return (
      <div className="container">
        <button 
          className="btn btn-danger" 
          onClick={(e) => changeVote(() =>{
            e.persist();
            e.preventDefault();
            if(!sessionStorage.getItem('userId')) {
              alert('Must be logged in to vote');
              return votes;
            }
            let userId = sessionStorage.getItem('userId');
            votePost({ variables: {
              postId: props.postId,
              user: userId,
            }});
            return votes
          })}>
            {votes}
          </button>
      </div>
  )
    
}