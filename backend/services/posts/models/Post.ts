export interface Post {
	id?: string;
	text: string;
	user: string;
	creationDate: Date;
	answers: Post[];
	voted: string[];
	parent: string;
}