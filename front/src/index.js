import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {
  BrowserRouter
} from "react-router-dom";

import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { ApolloProvider } from '@apollo/react-hooks';
import { gatewayAddress } from './config.json';

const cache = new InMemoryCache();
const link = new HttpLink({
  uri: gatewayAddress
})

const client = new ApolloClient({
  cache,
  link
})

ReactDOM.render(<ApolloProvider client={client}>  <BrowserRouter><App /></BrowserRouter></ApolloProvider>, document.getElementById('root'));

serviceWorker.unregister();
