import { Post as IPost } from '../models/Post';
import { DocumentStore, IDocumentSession, QueryStatistics } from 'ravendb';
import User from '../../users/entities/User';
import { ravenAddress } from '../index';

export default class Post implements IPost {
  id?: string;
  text: string;
  user: string;
  creationDate: Date;
  likes: number;
  answers: IPost[];
  voted: string[];
  parent: string;

  constructor(post: Post) {
    Object.assign(this, post);
  }

  static async findAll(session: IDocumentSession, pageSize: number, pageNumber: number) {
    let stats: QueryStatistics | undefined;
    const page = await session.query<Post>(Post)
      .whereEquals('parent', '')
      .skip(pageNumber * pageSize)
      .take(pageSize)
      .statistics((s) => {
          stats = s
      })
      .orderByDescending('creationDate')
      .all();

    page.map(p => 
      Object.assign(p, 
        {
          creationDate: p.creationDate.toDateString() + ' ' + p.creationDate.toLocaleTimeString(),
          user: p.user ? p.user : ''
        },
      )
    );

    return {
      page: page,
      totalCount: stats ? stats.totalResults : 0,
      totalPages: Math.ceil(stats.totalResults / pageSize),
    };
  }

  static async fetchById(id: string, session: IDocumentSession): Promise<Post> {
    return session.load<Post>(id);
  }

  static async createPost(input: Omit<Post, 'id' | 'creationDate' | 'answers' | 'likes' | 'voted'>, 
    session: IDocumentSession): Promise<Post> {
      const now =  new Date();
      const post = new Post({
          creationDate: now,
          id: null,
          answers: [],
          voted: [],
          likes: 0,
          parent: '',
          ...input,
      });
      await session.store(post);
      await session.saveChanges();
      return post;
  }

  static async getUser(userId: string) {
    const documentStore = new DocumentStore(ravenAddress, 'bexs-users');
    documentStore.initialize();
    const session = documentStore.openSession();
    const user = await session.load<User>(userId);
    if(!user) return '';
    return user.username;
  }

  static async getChildren(id: string, session: IDocumentSession): Promise<Post[]> {
    return session.query<Post>(Post)
      .whereEquals('parent', id)
      .all();
  }

  static async votePost(id: string, user: string, session: IDocumentSession): Promise<Post> {
    const post = await session.load<Post>(id);
    if(!post) throw Error('Post doesnt exist!');
    const userExists = await this.userExists(user);
    if(!userExists) throw Error('Invalid user');

    if(!post.voted) post.voted = new Array<string>();

    if(post.voted.includes(user)) {
      post.likes -= 1;
      post.voted.splice(post.voted.indexOf(user), 1);
    } else {
      post.likes += 1;
      post.voted.push(user);
    }
    
    await session.store(post);
    session.saveChanges();
    return post;
  }

  static async userExists(userId: string) {
    const documentStore = new DocumentStore(ravenAddress, 'bexs-users');
    documentStore.initialize();
    const session = documentStore.openSession();
    const userExists = await session.advanced.exists(userId);
    return userExists;
  }

}