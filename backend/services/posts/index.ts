import { ApolloServer, gql }  from 'apollo-server';
import { buildFederatedSchema } from '@apollo/federation';
import {  } from 'apollo-server'
import { createRavenDatabase, getStore } from '../../utils/dbUtils';
import RavenDBDataSource from '../../utils/dataSource';
import PostEntity from './entity/Post';

export const ravenAddress = process.env.RAVEN_ADDRESS 
  ? (process.env.RAVEN_ADDRESS)  
  : 'http://localhost:8080';

export const databaseName = process.env.DATABASE_NAME ? process.env.DATABASE_NAME : 'bexs-posts'

const typeDefs = gql`
  type Post {
    id: ID!
    text: String!
    parent: String
    creationDate: String!
    user: User @provides(fields: "username")
    children: [Post!]!
    likes: Int!
  }

  type PostConnection {
    totalCount: Int
    page: [Post]!
    totalPages: Int
  }


  extend type User @key(fields: "id") {
    id: ID! @external
    username: String @external
    post: [Post]
  }

  input CreatePostInput {
    text: String!
    user: String
    parent: String
  }

  input VotePostInput {
    postId: String!
    user: String!
  }

  type Mutation {
    createPost (post: CreatePostInput!): Post!
    votePost (input: VotePostInput!): Post!
  }

  type Query {
    post (id: String!): Post!
    paginatedPosts (
      pageSize: Int
      pageNumber: Int
    ): PostConnection!
  }
`;

const resolvers = {
  Query: {
    post: async (_, { id }, { dataSources: { ravenDataSource } }) => {
      const post = await PostEntity.fetchById(id, ravenDataSource.getSession());
      return {...post, creationDate: post.creationDate.toDateString() + ' ' + post.creationDate.toLocaleTimeString(), };
    },
    paginatedPosts: async (_, { pageSize = 20, pageNumber }, { dataSources: { ravenDataSource } }) =>
      await PostEntity.findAll(ravenDataSource.getSession(), pageSize, pageNumber),
  },
  Post: {
    children: async ({ id }, _, { dataSources: { ravenDataSource } }) =>
      await PostEntity.getChildren(id, ravenDataSource.getSession()),
  },
  User: {
    username(user) {
      if(user) {
        return PostEntity.getUser(user);
      }
      return '';
    }
  },
  Mutation: {
    createPost: (_,  {post} , { dataSources: { ravenDataSource } }) => {
      return  PostEntity.createPost(post, ravenDataSource.getSession())
    },
    votePost: (_,  { input } , { dataSources: { ravenDataSource } }) => {
      return  PostEntity.votePost(input.postId, input.user, ravenDataSource.getSession())
    },
  },
};

createRavenDatabase(ravenAddress, databaseName);
const store = getStore(ravenAddress, databaseName);

const server = new ApolloServer({
  dataSources: () => { 
    return { ravenDataSource: new RavenDBDataSource(store)}
  },
  schema: buildFederatedSchema([
    {
      typeDefs,
      resolvers
    }
  ])
});

const host = process.env.POSTS_HOST ? process.env.POSTS_HOST : 'localhost'
const port = process.env.POSTS_PORT ? process.env.POSTS_PORT : 4001

server.listen({ host, port }).then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
