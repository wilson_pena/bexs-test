import React from 'react';
import './App.css';
import {
  Switch,
  Route,
  Link,
  useLocation
} from "react-router-dom";

import 'bootstrap/dist/css/bootstrap.min.css';
import MainPage from './components/mainPage/MainPage';
import PostDetail from './components/postDetail/PostDetail';
import NewPost from './components/newPost/NewPost';

function App() {

  const location = useLocation();

  return (
    <React.Fragment>
    <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light" style={{position: 'sticky', paddingBottom: '0px'}}>
      <div className="row">
        <div className="col-1">
          <img width="100%" src="./logo.png" alt=""/>
        </div>
        <div className="col-7">
          <div className="main-header-label">Post-it</div>
        </div>
        <div className="col-2">
          <div style={{fontSize: '12px'}}>
            {sessionStorage.getItem('username') &&
              'Logged in as ' + sessionStorage.getItem('username')}
          </div>
        </div>
        </div>
        
    </nav>
    <nav className="navbar navbar-expand-lg navbar-light bg-light" style={{position: 'sticky', paddingBottom: '0px'}}>
      <div className="navbar-nav">
          <div style={{margin: '10px'}}>
            <Link to="/">
              <button type="button" className="btn-sm btn-dark">
                Home Page 
              </button> 
            </Link>
          </div>
          <div style={{margin: '10px'}}>
          {location.pathname !== '/new-post' &&
            <Link to="/new-post">
              <button type="button" className="btn-sm btn-primary">
                New Post
              </button>
            </Link>
          }
          </div>
      </div>
    </nav>
    <div className="container">
      <Switch>
        <Route path="/post-detail" render={props=> (
          <PostDetail {...props} ></PostDetail>
        )}>
        </Route>
        <Route path="/new-post" render={props=> (
          <NewPost ></NewPost>
        )}>
        </Route>
        <Route path="/">
          <MainPage/>
        </Route>
      </Switch>
      
    </div>
  </React.Fragment>
  );
}

export default App;
