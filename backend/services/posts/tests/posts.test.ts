
import { createRavenDatabase, getStore, deleteDatabase } from '../../../utils/dbUtils';
import PostEntity from '../entity/Post';
import { SinonFakeTimers, useFakeTimers } from 'sinon';

const testDatabaseAddress = 'http://localhost:8080' ;
const databaseName = 'bexs-test';

beforeAll(async () => {
  await createRavenDatabase(testDatabaseAddress, databaseName);
})

afterAll(async () => {
  await deleteDatabase(testDatabaseAddress, databaseName);
})

describe('test posts', () => {
  const store = getStore(testDatabaseAddress, databaseName);
  const mockDate = new Date();
  let clock: SinonFakeTimers = useFakeTimers(mockDate);

  afterEach(() => {
    clock.restore();
  })
  
  afterAll(() => {
    store.dispose();
  })

  test('create new post', async () => {
    const session = store.openSession()

    const newPost = await PostEntity.createPost({
        text: 'New Post 1', 
        user: 'me',
        parent: ''
      }, 
      session);
    session.dispose();

    const Post = {
      text: 'New Post 1',
      answers: [],
      id: "posts/1-A",
      creationDate: mockDate as Date,
      user: 'me',
    }

    expect(newPost).toMatchObject(Post);
  });

  test('get by id', async () => {
    const session = store.openSession()

    const post = await PostEntity.fetchById("posts/1-A", session);
    session.dispose();
    
    const Post = {
      text: 'New Post 1',
      answers: [],
      id: "posts/1-A",
      creationDate: mockDate as Date,
      user: 'me',
    }

    expect(post).toMatchObject(Post);
  });

  test('get by invalid id', async () => {
    const session = store.openSession()
    const fetchedPost = await PostEntity.fetchById("posts/2-A", session);
    session.dispose();

    expect(fetchedPost).toBeNull();
  });

  test('vote with empty user id', async() => {
    const session = store.openSession()
    expect(async () => {
      await PostEntity.votePost('posts/1-A', '', session);
    }).rejects.toThrow(new Error('id cannot be null'))
    session.dispose();
  });

  test('vote with invalid user', async() => {
    const session = store.openSession()
    await expect(async () => {
      await PostEntity.votePost('posts/1-A', 'invalidUserId', session);
    }).rejects.toThrow(new Error('Invalid user'))
    session.dispose();
  });
  
})
