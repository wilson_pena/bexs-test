import React, { useState } from 'react';
import { useMutation } from '@apollo/react-hooks';
import gql from "graphql-tag";

const LOG_IN = gql`
mutation LogInAs($username: String!, $password: String!) {
  logInAs(
    username: $username
    password: $password
    ) {
    id
    username
  }
}
`;


export default function UserLogin() {

  let [ user, submitLogin ] = useState({});

  const [
    logIn,
  ] = useMutation(LOG_IN, {
    onCompleted( { logInAs } ) {
      sessionStorage.setItem('username', logInAs.username);
      sessionStorage.setItem('userId', logInAs.id)
      window.location.reload(); 
    },
    onError( err ) {
      alert(err.message)
    }
  });

  return (
      <div className="userLogin row navbar navbar-light bg-light">
        {!sessionStorage.getItem('userId') && 
          <div className="col-12">
              <div className="row" style={{justifyContent: 'center'}}>
                  <div>Log in as:</div>
              </div>
              <div className="form-group">
                  <label><small className="form-text text-muted">Username</small></label>
                  <input 
                    onChange={(event) => submitLogin(() => {
                      event.persist();
                      if(!event.target) return user;
                      user.username= event.target.value;
                      return user;
                    })}
                    type="text" 
                    className="form-control-sm" 
                    placeholder="Username"
                  />
              </div>
              <div className="form-group">
                  <label><small className="form-text text-muted">Password</small></label>
                  <input 
                    onChange={(event) => submitLogin(() => {
                      event.persist()
                      if(!event.target) return user;
                      user.password= event.target.value
                      return user
                    })}
                    type="password" 
                    className="form-control-sm" 
                    placeholder="Password"
                  />
              </div>
              <div className="row" style={{justifyContent: 'center'}}>
                <button 
                  type="button" 
                  className="btn btn-primary"  
                  onClick={(e) => submitLogin(() => {
                    e.preventDefault();
                    if(!user || !user.password || !user.username) {
                      alert('Missinge fields');
                      return user;
                    }
                    logIn({ variables: {
                      username: user.username,
                      password: user.password,
                    }});
                    return user;
                  })}>
                  Log In
                </button>
              </div>
          </div>
        }
        {sessionStorage.getItem('userId') && 
          <div className="col-12">
            <div className="row" style={{justifyContent: 'center'}}>
              <button className="btn btn-warning" onClick={(e) => {
                sessionStorage.removeItem('userId');
                sessionStorage.removeItem('username');
                window.location.reload();
              }}>
                Logout
              </button>
            </div>
          </div>
        }
      </div>
  )
    
}