# Post-it

O Post-it é uma aplicação web de perguntas e respostas.

## Funcionalidades

- Login de usuários
- Postar perguntas
- Responder perguntas de outros usuários
- Dar _likes_ em postagens
- Ordenação de posts de acordo com a data de postagem
- Paginação de posts

## Tecnologias

- [Apollo](https://www.apollographql.com/): Implementação de gateway [GraphQL](https://graphql.org/), unindo os microsserviços através do esquema de [Federation do Apollo](https://www.apollographql.com/docs/apollo-server/federation/introduction/)
- [React](https://reactjs.org/): Para criação do frontend em conjunto com bibliotecas do `Apollo`.
- [RavenDB](https://ravendb.net/):  base de dados utilizada é `NoSQL`, o `RavenDB` para demonstrar um web app que tenha uma grande quantidade de usuários e posts de forma volátil e com boa performance.
- [Jest](https://jestjs.io/): Para execução de testes unitários.
- [Lerna](https://github.com/lerna/lerna): Biblioteca de JS/Typescript para rodar microsserviços de forma facilitada.
- [NodeJS](https://nodejs.org/en/)
- [Docker](https://www.docker.com/): Para facilitar a execução do app.

## Utilização

#### Postando

Clicar no botão `New Post` para postar novas perguntas ou clique em uma pergunta já feita e em seguida em `Responder` para responder aquela pergunta.

Respostas á postagens também podem ter resposta e receber `likes`.

Os posts são ordanados por data de criação na `Home Page`. **e são paginados**. A paginação default é 10 por página, podendo ser modificada para `5`, `10` ou `20`.

#### Login

O login é bem simples. No canto direito existe um componente responsável pelo login do usuário, nele são colocados um username e uma senha.

Ao clicar no botão `Log In` se o username não existir um novo usuário será criado, caso contrário será analisado se a senha é a mesma deste username. Caso não seja um `alert` será mostrado na tela e o usuário não será logado.

No caso de sucesso (criação ou login de usuário já criado) será gravado na `sessionStorage` do Browser.

> Foi testado apenas no Chrome o funcionamento!

Postagens feitas por usuários logados ficam com uma tag azul mostrando o username de quem postou, postagens feitas sem login ficam como `Anonymous`.

#### Likes

Foi implementado um botão de `like` no projeto. **Que só pode ser usado por usuários logados** e pode ser votado uma vez apenas por posta/usuário.

Ele pode ser clicado novamente para remover o `like` do post.

## Subindo a aplicação

#### Via Docker

1. Executar o comando `docker-compose build` na raiz do projeto para buildar as imagens.

2. Executar o comando `docker-compose up`.

3. A aplicação estará executando em http://localhost:3000.

> Caso sua aplicação ``gateway`` não esteja executando no ``localhost`` configurar o apontamento do front para o endereço correto, através do comando:
>
> ``docker-compose build --build-arg GATEWAY_ADDRESS=http://<domínio do gateway>:4000``

#### Via NPM (Local)

1. Instalar versão `12.x.x` do [NodeJS](https://nodejs.org/en/) na máquina.

2. Usar o comando `npm install` na pasta `./backend` do projeto.

3. Rodar uma instância do ``RavenDB`` na porta ``8080``

      - caso não tenha RavenDB na máquina vá na raiz do projeto e use o comando `docker-compose up raven-db` (**caso tenha o docker instalado nesta máquina**)

4. Iniciar serviços NodeJS com o comando `npm run start-services` na pasta `./backend`.

5. Iniciar o gateway do Apollo com o comando `npm run start-gateway` no diretório `./backend`

6. Executar comandos `npm install` e `npm start` na pasta `./front` para iniciar o serviço React.

7. Ir até http://localhost:3000 e começar a postar!

### API - Mutations e Queries

A API foi feita em `GraphQL` usando o framework `Apollo`.

No microserviço `posts` temos as queries e mutações:

```graphql
  type Mutation {
    createPost (post: CreatePostInput!): Post! # Para criar post
    votePost (input: VotePostInput!): Post! # Para dar like ou tirar o like
  }

  type Query {
    post (id: String!): Post! # Buscar post por id
    paginatedPosts (
      pageSize: Int
      pageNumber: Int
    ): PostConnection!  # Busca paginada de posts
  }
```

No serviço de `users` temos a seguinte mutação que faz o login do usuário:

```graphql
  type Mutation {
    logInAs (username: String!, password: String!): User!
  }

```

## Testes

Para rodar os testes unitários vá até o diretório `backend`, faça `npm install` para instalar as dependèncias, e rode o comando `npm run test` para rodar os testes unitários dos microserviços.

## Arquitetura

### Modelos e Entidades

A estrutura das entidades está mostrada abaixo. Foram criadas duas entidades principais `Post` e `User`. Sendo a entidade `User` criada **por questão de demonstração** de como seria um microserviço que delegaria a questão de Login, criação de usuários, autenticação, etc.

`Post:`

```json
{
  "id": 123,
  "text": "My question",
  "user": {
    "username": "myUsername1"
  },
  "likes": 1,
  "creationDate": "2020-01-01 12:00:00",
  "children": [
    {
	  "id": 1234,
	  "text": "My answer",
	  "user": "another.username",
	  "creationDate": "2020-01-01 12:00:00"
	}
  ]
}
```

`User:`

```json
{
  "id": 123,
  "username": "myUsername1",
  "password": "mypass123"
}
```
