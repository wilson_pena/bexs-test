import { User as IUser } from '../models/User';
import { IDocumentSession } from 'ravendb';

export default class User implements IUser {
  id: string;
  username: string;
  password: string;

  constructor(user: User) {
    Object.assign(this, user);
  }

  static async fetchById(id: string, session: IDocumentSession): Promise<User> {
    return session.load<User>(id);
  }

  static async createUser(username: string, password: string, session: IDocumentSession): Promise<User> {
    const checkUsername = await session
      .query<User>(User)
      .containsAny('username', [username])
      .firstOrNull();
    
    if (checkUsername) {
      if(checkUsername.password != password) throw Error('Invalid password!');
      else return checkUsername;
    }

    const user = new User({
      id: null,
      username: username,
      password: password,
    });
    await session.store(user);

    await session.saveChanges();
    return user;
  }
}